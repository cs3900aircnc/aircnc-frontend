import { UPDATE_HOUSE, RESET_DATA } from "./SearchAction";

export default (
  state = {
    present: [],
    more: true,
    pageStart: 0
  },
  action
) => {
  switch (action.type) {
    case UPDATE_HOUSE:
      const { present, more } = action.payload;
      return { ...state, present, more };
    case RESET_DATA:
      const pageStart = state.pageStart === 0 ? -1 : 0;
      return { present: [], more: true, pageStart };
    default:
      return state;
  }
};
