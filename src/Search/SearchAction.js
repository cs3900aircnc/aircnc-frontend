import axios from "axios";

export const UPDATE_HOUSE = "UPDATE_HOUSE";
export const RESET_DATA = "RESET_DATA";

export const numberOfLoading = 20;

export const loadItem = (url, page) => dispatch => {
  axios
    .get(url)
    .then(response => {
      const maxLength = response.data.length;
      const end =
        page * numberOfLoading > maxLength ? maxLength : page * numberOfLoading;

      const more = !(end >= maxLength);
      dispatch({
        type: UPDATE_HOUSE,
        payload: {
          present: response.data.slice(0, end),
          more
        }
      });
    })
    .catch(error => {
      console.log(error);
    });
};

export const reset = () => dispatch => {
  dispatch({ type: RESET_DATA });
}
