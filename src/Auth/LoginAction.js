import axios from "axios";
import getToken from './GetToken';

export const START_LOGIN = "START_AUTOLOGIN";
export const LOGIN_FAILED = "AUTOLOGIN_FAILED";
export const LOGIN_SECCUSS = "AUTOLOGIN_SECCUSS";
export const LOG_OUT = "LOG_OUT";
export const CHANGE_SIGNIN_MODAL = "CHANGE_SIGNIN_MODAL";
export const CHANGE_SIGNUP_MODAL = "CHANGE_SIGNUP_MODAL";
export const UPDATE_AVATAR = "UPDATE_AVATAR";

export const autoLogin = () => dispatch => {
  dispatch({ type: START_LOGIN });

  const token = localStorage.getItem("token");
  const access_token = localStorage.getItem("access_token");

  if (token) {
    const onSuccess = response => {
      const { username, pk, iconUrl } = response.data;
      dispatch({ type: LOGIN_SECCUSS, payload: { username, pk, iconUrl } });
      refreshToken();
    };

    const onFail = error => {
      localStorage.removeItem("token");
      dispatch({ type: LOGIN_FAILED });
    };

    axios
      .post("/api-token-verify/", { token })
      .then(onSuccess)
      .catch(onFail);
  } else if (access_token) {
    axios
      .get("/current_user/", {
        headers: { Authorization: `Bearer ${access_token}` }
      })
      .then(response => {
        const { username, pk, iconUrl } = response.data;
        dispatch({ type: LOGIN_SECCUSS, payload: { username, pk, iconUrl } });
      })
      .catch(err => {
        localStorage.removeItem("access_token");
        localStorage.removeItem("refresh_token");
        dispatch({ type: LOGIN_FAILED });
      });
  } else {
    dispatch({ type: LOGIN_FAILED });
  }
};

export const logout = () => dispatch => {
  localStorage.removeItem("token");
  localStorage.removeItem("access_token");
  localStorage.removeItem("refresh_token");
  clearInterval(localStorage.getItem("intervalID"));
  localStorage.removeItem("intervalID");
  dispatch({ type: LOG_OUT });
};

export const login = (username, password, setError, loadStatus) => dispatch => {
  const userLogin = { username, password };

  const onSuccess = response => {
    localStorage.setItem("token", response.data.token);
    loadStatus(false);
    changeSigninModal(false);
    const { username, pk, iconUrl } = response.data;
    dispatch({ type: LOGIN_SECCUSS, payload: { username, pk, iconUrl } });
    refreshToken();
  };

  const onFail = error => {
    loadStatus(false);
    setError(error);
    dispatch({ type: LOGIN_FAILED, payload: error });
  };

  axios
    .post("/api-token-auth/", userLogin)
    .then(onSuccess)
    .catch(onFail);
};

export const signUp = (newUser, setError, loadStatus) => dispatch => {
  const onSuccess = response => {
    localStorage.setItem("token", response.data.token);
    loadStatus(false);
    changeSignUpModal(false);
    const { username, pk } = response.data;
    dispatch({
      type: LOGIN_SECCUSS,
      payload: { username, pk, iconUrl: "" }
    });
    refreshToken();
  };

  const onFail = error => {
    console.log(error);
    loadStatus(false);
    setError(error);
    dispatch({ type: LOGIN_FAILED, payload: error });
  };

  axios
    .post("/register/", newUser)
    .then(onSuccess)
    .catch(onFail);
};

const refreshToken = () => {
  const intervalID = setInterval(() => {
    const token = localStorage.getItem("token");
    axios
      .post("/api-token-refresh/", { token: token })
      .then(response => {
        localStorage.setItem("token", response.data.token);
      })
      .catch(error => {
        console.log(error.response);
      });
  }, 40000);

  localStorage.setItem("intervalID", intervalID);
};

export const changeSigninModal = status => dispatch => {
  dispatch({ type: CHANGE_SIGNIN_MODAL, payload: status });
};

export const changeSignUpModal = status => dispatch => {
  dispatch({ type: CHANGE_SIGNUP_MODAL, payload: status });
};

export const updateAvatar = (url, finishLoading) => dispatch => {
  const token = getToken();
  axios
    .post(
      "/updateProfile/",
      { iconUrl: url },
      {
        headers: { Authorization: token }
      }
    )
    .then(response => {
      dispatch({ type: UPDATE_AVATAR, payload: response.data.iconUrl });
      finishLoading();
    })
    .catch(err => {
      console.log(err);
      finishLoading();
    });
};

export const facebookLogin = accessToken => dispatch => {
  dispatch({ type: START_LOGIN });

  const body = {
    client_id: "M7CKWBCr75t9cwxLojc94e0S7fGWR61qkLu6eeMd",
    client_secret:
      "tVm1xkohWwcxrb3kS8wbgL1IlfyxXRYQdWNe1TfCXk6m0ZKbrQKbnincUttQtw4zwqd8Ft8xOgPZIiTYntPwBsqwmxNFC1khXaOfI30AsGV2GggTkYRYnkvqWcRlrKNv",
    backend: "facebook",
    grant_type: "convert_token",
    token: accessToken
  };

  new Promise(resolve => {
    axios
      .post("/auth/convert-token/", body)
      .then(response => {
        const { access_token, refresh_token } = response.data;
        localStorage.setItem("access_token", access_token);
        localStorage.setItem("refresh_token", refresh_token);
        resolve(access_token);
      })
      .catch(err => {
        console.log(err.response);
      });
  }).then(access_token => {
    axios
      .get("/current_user/", {
        headers: { Authorization: `Bearer ${access_token}` }
      })
      .then(response => {
        dispatch({ type: LOGIN_SECCUSS, payload: response.data });
      })
      .catch(err => {
        dispatch({ type: LOGIN_FAILED });
        console.log(err.response);
      });
  });
};
