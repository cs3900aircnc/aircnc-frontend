import {
  START_LOGIN,
  LOGIN_FAILED,
  LOGIN_SECCUSS,
  LOG_OUT,
  CHANGE_SIGNIN_MODAL,
  CHANGE_SIGNUP_MODAL,
  UPDATE_AVATAR
} from "./LoginAction";

export default (
  state = {
    login: false,
    loading: true,
    error: null,
    name: null,
    signinModal: false,
    signUpModal: false,
    avatarurl: "",
    userId: ""
  },
  action
) => {
  switch (action.type) {
    case START_LOGIN: {
      return { ...state, loading: true, error: null };
    }
    case LOGIN_FAILED: {
      return {
        ...state,
        loading: false,
        login: false,
        error: action.payload,
        avatarurl: ""
      };
    }
    case LOGIN_SECCUSS: {
      const { username, pk, iconUrl } = action.payload;
      return {
        ...state,
        loading: false,
        login: true,
        name: username,
        error: null,
        avatarurl: iconUrl,
        userId: pk
      };
    }
    case LOG_OUT: {
      return {
        ...state,
        loading: false,
        login: false,
        name: null,
        error: null,
        avatarurl: ""
      };
    }
    case CHANGE_SIGNIN_MODAL: {
      return { ...state, signinModal: action.payload };
    }
    case CHANGE_SIGNUP_MODAL: {
      return { ...state, signUpModal: action.payload };
    }
    case UPDATE_AVATAR: {
      return { ...state, avatarurl: action.payload };
    }
    default:
      return state;
  }
};
