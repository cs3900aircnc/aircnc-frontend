export default function getToken() {
  const token = localStorage.getItem("token");
  const access_token = localStorage.getItem("access_token");
  if (token) {
    return `JWT ${token}`;
  } else if (access_token) {
    return `Bearer ${access_token}`;
  } else {
    return null;
  }
}