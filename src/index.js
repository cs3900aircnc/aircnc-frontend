import React from "react";
import ReactDOM from "react-dom";
import registerServiceWorker from "./registerServiceWorker";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import axios from "axios";
import { LocaleProvider } from "antd";
import enUS from "antd/lib/locale-provider/en_US";

import "./style";

import App from "./App/App";
import rootReducer from "./rootReducer";

const store = createStore(rootReducer, applyMiddleware(thunk));

// axios.defaults.baseURL = "https://backend.aircnc.ga";
axios.defaults.baseURL = "http://127.0.0.1:8000/";

ReactDOM.render(
  <Provider store={store}>
    <LocaleProvider locale={enUS}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </LocaleProvider>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
