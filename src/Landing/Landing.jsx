import React, { Component } from "react";
import HouseCard from "./HouseCard";
import { Spin } from "antd";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import _ from "lodash";
import qs from "querystringify";
import MyScroll from "./MyScroll";

import { checkURL } from "../Navigation/NavStyleAction";
import { loadItem, reset } from "../Search/SearchAction";

class Landing extends Component {
  constructor(props) {
    super(props);
    this.actions = bindActionCreators(
      { checkURL, loadItem, reset },
      this.props.dispatch
    );
  }

  componentDidMount() {
    this.actions.checkURL(this.props.location.pathname);
  }

  loadItem = page => {
    let query = qs.parse(this.props.location.search);
    query = qs.stringify(_.omit(query, ["children", "adults", "infants"]));
    const url = `/search/housings?${query}`;
    this.actions.loadItem(url, page);
  };

  render() {
    const { more, present, pageStart } = this.props;

    return (
      <div
        style={{
          paddingLeft: 45,
          paddingRight: 45,
          paddingBottom: 100,
          marginTop: 120
        }}
      >
        <MyScroll
          pageStart={pageStart}
          loadMore={this.loadItem}
          hasMore={more}
          loader={
            <Spin
              size="large"
              key={0}
              style={{ margin: "auto", paddingTop: 50 }}
            />
          }
          className="row"
        >
          {present.map(house => (
            <HouseCard key={house.pk} house={house} />
          ))}
        </MyScroll>

        {!more && (
          <div style={{ textAlign: "center", fontSize: 20, marginTop: 60 }}>
            No more result
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { more, present, pageStart } = state.search;
  return { more, present, pageStart };
};

export default _.flow(
  withRouter,
  connect(mapStateToProps)
)(Landing);
