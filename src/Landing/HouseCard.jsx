import React, { Component } from "react";
import { Card, Rate } from "antd";
import HouseImage from "./HouseImage";
import { withRouter } from "react-router-dom";

const truncate = s => {
  return s.length >= 45 ? s.slice(0, 45) + " ..." : s;
};

class HouseCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pk: null,
      name: null,
      rating: null,
      price: null,
      images: [],
      loading: true
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    const { pk, images } = this.props.house;
    const { listing_name, price, rating } = this.props.house;
    this.setState({
      pk,
      name: listing_name,
      price,
      rating,
      images,
      loading: false
    });
  }

  checkHouse = () => {
    const { pk } = this.state;
    this.props.history.push(`/house/${pk}`);
  };

  render() {
    const { pk, images, price, rating, name } = this.state;
    return (
      <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6">
        <div style={{ marginTop: 20 }}>
          <Card
            cover={<HouseImage pk={pk} images={images} />}
            hoverable={true}
            bodyStyle={{
              paddingTop: 10,
              paddingBottom: 5,
              height: 110,
              paddingLeft: 10
            }}
            bordered={false}
            style={{ borderRadius: 5 }}
            loading={this.state.loading}
          >
            {!this.state.loading && (
              <div onClick={this.checkHouse}>
                <h6 style={{ overflowWrap: "break-word" }}>{truncate(name)}</h6>
                <div>${price} AUD per night</div>
                <Rate
                  disabled
                  defaultValue={Number(rating)}
                  allowHalf
                  style={{ height: 5, fontSize: 12 }}
                />
              </div>
            )}
          </Card>
        </div>
      </div>
    );
  }
}

export default withRouter(HouseCard);
