import React, { Component } from "react";
import NavBar from "../Navigation/NavBar";
import Routes from "./Routes";

class App extends Component {
  render() {
    return (
      <div>
        <NavBar />
        <main className="container-fluid" style={{ marginTop: 20 }}>
          <Routes />
        </main>
      </div>
    );
  }
}

export default App;
