import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import moment from "moment";
import qs from "querystringify";

import Landing from "../Landing/Landing";
import House from "../House/House";
import ProfileNavigation from "../Profile/ProfileNavigation";

const query = qs.stringify({
  search: "",
  startDate: moment().format("YYYY-MM-DD"),
  endDate: moment()
    .add(1, "days")
    .format("YYYY-MM-DD")
});

export default () => (
  <Switch>
    <Route
      exact
      path="/"
      component={() => <Redirect to={`/search/housings?${query}`} />}
    />
    <Route exact path="/search/housings" component={Landing} />
    <Route exact path="/house/:pk" component={House} />
    <Route exact path="/myprofile" component={ProfileNavigation} />
    <Route exact path="/myhouse" component={ProfileNavigation} />
    <Route exact path="/mybooking/host" component={ProfileNavigation} />
    <Route exact path="/mybooking/guest" component={ProfileNavigation} />
    <Route exact path="/updatehouse/:id" component={ProfileNavigation} />
  </Switch>
);
