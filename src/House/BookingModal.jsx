import React, { Component } from "react";
import { Modal, Input, Form, Button, notification } from "antd";
import { connect } from "react-redux";
import _ from "lodash";
import axios from "axios";
import getToken from "../Auth/GetToken";
import { withRouter } from "react-router-dom";

const { TextArea } = Input;
const FormItem = Form.Item;

class BookingModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false
    };
  }

  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFields((err, value) => {
      if (err) return;

      const { dateRange, price, cancel, host, guestId, house } = this.props;

      const hostId = host.pk;

      const days =
        dateRange.length === 0 ? 0 : dateRange[1].diff(dateRange[0], "days");
      const start = dateRange[0].format();
      const end = dateRange[1].format();
      const startDate = start.substring(0, start.length - 6) + "Z";
      const endDate = end.substring(0, end.length - 6) + "Z";

      const token = getToken();

      const newBooking = {
        host: hostId,
        guest: guestId,
        price: days * Number(price),
        startDate,
        endDate,
        status: "P",
        house: house,
        message: value.message
      };

      this.setState({ loading: true });
      axios
        .post("/bookings/", newBooking, {
          headers: { Authorization: token }
        })
        .then(response => {
          this.setState({ loading: false });
          cancel();
          notification["success"]({
            message: "New Order",
            description:
              "Your order has been placed! Please wait until host confirm your order!",
            style: { marginTop: 53 }
          });
        })
        .catch(err => {
          console.log(err.response);
          this.setState({ loading: false });
        });
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { visible, cancel, dateRange, price } = this.props;

    const { loading } = this.state;

    const days =
      dateRange.length === 0 ? 0 : dateRange[1].diff(dateRange[0], "days");

    const total = days * Number(price);

    return (
      <Modal visible={visible} onCancel={cancel} footer={null}>
        <h2> $ {total} AUD</h2>
        {dateRange.length !== 0 && (
          <div style={{ marginBottom: 15 }}>
            {dateRange[0].format("ll")} - {dateRange[1].format("ll")}
          </div>
        )}

        <Form onSubmit={this.handleSubmit}>
          <FormItem label="Message to your host:">
            {getFieldDecorator("message", {
              rules: [
                {
                  required: true,
                  message: "Please leave a message to your host!"
                }
              ]
            })(<TextArea rows={6} disabled={loading} />)}
          </FormItem>

          <FormItem>
            <Button
              type="dashed"
              block
              onClick={cancel}
              loading={loading}
              disabled={loading}
            >
              Cancel
            </Button>
          </FormItem>

          <FormItem>
            <Button
              type="primary"
              htmlType="submit"
              block
              loading={loading}
              disabled={loading}
            >
              Booking
            </Button>
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

const mapPropsToState = state => {
  return { guestId: state.auth.userId };
};

export default _.flow(
  withRouter,
  Form.create(),
  connect(mapPropsToState)
)(BookingModal);
