import React, { Component } from "react";
import axios from "axios";

const key = "AIzaSyC9J6R6ITBAEXTsHFCPnd4xU7xiCDYX-7Q";

export default class Map extends Component {
  componentDidMount() {
    this.fetchPosition().then(pos => {
      if (pos) {
        const google = window.google;
        const map = new google.maps.Map(this.mapContainer);
        map.setCenter(pos);
        map.setZoom(16);
        new google.maps.Marker({
          map: map,
          position: pos
        });
      }
    });
  }

  fetchPosition() {
    const { address } = this.props;
    return new Promise(resolve => {
      axios
        .get(
          `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${key}`
        )
        .then(response => {
          if (response.data.results.length === 0) {
            resolve(null);
          } else {
            resolve(response.data.results[0].geometry.location);
          }
        });
    });
  }

  render() {
    return (
      <div>
        <div
          ref={red => (this.mapContainer = red)}
          style={{ width: 580, height: 300 }}
        />
      </div>
    );
  }
}
