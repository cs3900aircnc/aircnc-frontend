import React, { Component } from "react";
import axios from "axios";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import _ from "lodash";
import Map from "./Map";
import moment from "moment";
import {
  Carousel,
  Card,
  DatePicker,
  Rate,
  Divider,
  Popover,
  Button,
  message,
  Avatar
} from "antd";

import BookingModal from "./BookingModal";

import * as checkURL from "../Navigation/NavStyleAction";
import { changeSigninModal } from "../Auth/LoginAction";

const { RangePicker } = DatePicker;

message.config({
  top: 80,
  duration: 2,
  maxCount: 1
});

class House extends Component {
  constructor(props) {
    super(props);

    this.state = {
      guests: 1,
      imgHeight: 700,
      houseData: {},
      visibility: false,
      adults: 1,
      children: 0,
      infants: 0,
      bookingVisible: false,
      date: [],
      pickerVisibility: false,
      dateRange: []
    };

    this.actions = bindActionCreators(
      { ...checkURL, changeSigninModal },
      this.props.dispatch
    );
  }

  componentDidMount() {
    this.actions.checkURL(this.props.location.pathname);
    window.scrollTo(0, 0);

    const { pk } = this.props.match.params;

    const onSuccess = response => {
      this.setState({ houseData: response.data });
    };

    const onFail = error => {
      console.log(error);
    };

    axios
      .get(`/rooms/${pk}/`)
      .then(onSuccess)
      .catch(onFail);

    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
  }

  updateWindowDimensions = () => {
    this.setState({ imgHeight: window.innerHeight * 0.65 });
  };

  guestsPopover = name => {
    const { guests, houseData } = this.state;
    const currentProp = name.toLowerCase();
    const current = this.state[currentProp];
    const marginTop = name === "Adults" ? 10 : 25;
    return (
      <div>
        <div
          style={{
            width: 385,
            display: "inline-block",
            fontSize: 16,
            marginTop
          }}
        >
          {name}
        </div>
        <Button
          className="minus"
          type="primary"
          shape="circle"
          icon="minus"
          ghost
          onClick={() =>
            this.setState({ [currentProp]: current - 1, guests: guests - 1 })
          }
          disabled={
            guests <= 1 || name === "Adults" ? current <= 1 : current <= 0
          }
        />
        <span
          style={{ textAlign: "center", width: 26, display: "inline-block" }}
        >
          {current}
        </span>

        <Button
          className="plus"
          type="primary"
          shape="circle"
          icon="plus"
          ghost
          onClick={() =>
            this.setState({ [currentProp]: current + 1, guests: guests + 1 })
          }
          disabled={guests >= houseData.guests}
        />
      </div>
    );
  };

  checkDateValid = dates => {
    if (dates.length === 0) {
      this.setState({ dateRange: [] });
      return;
    }

    const start = dates[0];
    const end = dates[1];

    if (start.diff(end) === 0) {
      this.setState({ dateRange: [] });
      return;
    }

    const { bookings } = this.state.houseData;
    for (let key in bookings) {
      const book = bookings[key];
      const { startDate, endDate } = book;
      if (
        moment(start).endOf("day") <= moment(startDate).endOf("day") &&
        moment(end).endOf("day") >= moment(endDate).endOf("day")
      ) {
        this.setState({ dateRange: [] });
        return;
      }
    }
    this.setState({ dateRange: dates });
  };

  checkValueDate = current => {
    const { bookings } = this.state.houseData;
    for (let key in bookings) {
      const book = bookings[key];
      const { startDate, endDate } = book;
      if (
        current >
          moment(startDate)
            .endOf("day")
            .add(-1, "day") &&
        current < moment(endDate).endOf("day")
      ) {
        return true;
      }
    }
    return false;
  };

  disabledDate = current => {
    const { startDate, endDate } = this.state.houseData;
    return (
      current <
        moment()
          .endOf("day")
          .add(-1, "day") ||
      current >
        moment(endDate)
          .endOf("day")
          .add(-1, "day") ||
      current < moment(startDate).endOf("day") ||
      this.checkValueDate(current)
    );
  };

  render() {
    const {
      imgHeight,
      houseData,
      visibility,
      infants,
      adults,
      children,
      bookingVisible,
      pickerVisibility,
      dateRange
    } = this.state;

    const { login } = this.props;
    const {
      address,
      user,
      listing_name,
      listing_description,
      beds,
      guests,
      baths,
      rules,
      price,
      pk
    } = houseData;

    return (
      <div style={{ marginTop: 80, marginBottom: 80 }}>
        <div className="row">
          <div className="col-lg-7">
            {houseData &&
              houseData.images && (
                <Carousel style={{ height: imgHeight }} autoplay>
                  {houseData.images.map((img, index) => (
                    <div key={index}>
                      <img
                        src={img}
                        style={{ width: "100%", height: imgHeight }}
                        alt=""
                      />
                    </div>
                  ))}
                </Carousel>
              )}
          </div>
          <div className=" col-lg-5 ">
            {houseData && (
              <Card
                style={{ width: "103%", height: imgHeight, marginLeft: -15 }}
              >
                <h2>
                  $
                  {dateRange.length === 0
                    ? houseData.price
                    : dateRange[1].diff(dateRange[0], "days") *
                      Number(houseData.price)}{" "}
                  AUD
                  <br />
                  {houseData.rating && (
                    <Rate
                      disabled
                      allowHalf
                      defaultValue={parseFloat(houseData.rating)}
                      style={{ fontSize: 15 }}
                    />
                  )}
                </h2>
                <h5> Date </h5>
                <RangePicker
                  format="ll"
                  style={{ marginBottom: 20, width: 534 }}
                  open={pickerVisibility}
                  onOpenChange={status => {
                    this.setState({ pickerVisibility: status });
                  }}
                  onChange={dates => this.checkDateValid(dates)}
                  value={dateRange}
                  disabledDate={this.disabledDate}
                />
                <h1> {pickerVisibility}</h1>
                <h5> Guests </h5>
                <Popover
                  placement="bottom"
                  trigger="click"
                  visible={visibility}
                  onVisibleChange={visible =>
                    this.setState({ visibility: visible })
                  }
                  content={
                    <div style={{ marginBottom: 15 }}>
                      {this.guestsPopover("Adults")}
                      {this.guestsPopover("Children")}
                      {this.guestsPopover("Infants")}
                      <div style={{ marginTop: 25, display: "flex" }}>
                        <div style={{ flex: 1 }} />
                        <Button
                          size="small"
                          onClick={() =>
                            this.setState({
                              adults: 1,
                              children: 0,
                              infants: 0
                            })
                          }
                        >
                          Clear
                        </Button>
                        <Button
                          style={{ marginLeft: 18 }}
                          size="small"
                          type="primary"
                          onClick={() => {
                            this.setState({ visibility: false });
                          }}
                        >
                          Apply
                        </Button>
                      </div>
                    </div>
                  }
                >
                  <Button block>
                    {`Adults ${adults},  Infants ${infants},  Children ${children}`}
                  </Button>
                </Popover>
                <Button
                  style={{ marginTop: 30, marginBottom: 15 }}
                  type="primary"
                  size="large"
                  block
                  onClick={() => {
                    if (!login) {
                      message.info("Plese login before booking the room !");
                      this.actions.changeSigninModal(true);
                    } else if (dateRange.length === 0) {
                      this.setState({ pickerVisibility: true });
                    } else {
                      this.setState({ bookingVisible: true });
                    }
                  }}
                >
                  Book Now !
                </Button>
                <h6 style={{ textAlign: "center" }}>
                  You won't be charged yet
                </h6>
                <Divider />
                <div>
                  Enter dates and number of guests to see the total trip price,
                  including additional fees and any taxes.
                </div>
              </Card>
            )}
          </div>
        </div>
        <Divider />
        {listing_name && <h4> {listing_name} </h4>}
        {user && (
          <div>
            <Avatar
              src={user.iconUrl}
              icon="user"
              size={50}
              style={{ marginRight: 10 }}
            />{" "}
            <i>Description from host: </i>
          </div>
        )}

        <div className="row">
          <div className="col-lg-7">
            <div style={{ marginLeft: 60 }}>
              {listing_description && <div>{listing_description}</div>}
              {beds &&
                guests &&
                baths && (
                  <div style={{ marginTop: 30 }}>
                    <span style={{ marginRight: 15 }}>
                      <i className="fas fa-user-friends fa-lg" /> {guests}{" "}
                      Guests
                    </span>

                    <span style={{ marginRight: 15 }}>
                      <i className="fas fa-bed fa-lg" /> {beds} Beds
                    </span>

                    <span>
                      <i className="fas fa-bath fa-lg" /> {baths} Baths
                    </span>
                  </div>
                )}

              <h6 style={{ marginTop: 30 }}>Rules</h6>
              {rules &&
                JSON.parse(rules).length !== 0 && (
                  <div>
                    {JSON.parse(rules).map((rule, index) => (
                      <div key={index}>{rule}</div>
                    ))}
                  </div>
                )}
            </div>
          </div>
          {address && <Map address={address} />}
        </div>

        <BookingModal
          visible={bookingVisible}
          cancel={() => this.setState({ bookingVisible: false })}
          dateRange={dateRange}
          price={price}
          host={user}
          house={pk}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { login } = state.auth;
  return { login };
};

export default _.flow(
  withRouter,
  connect(mapStateToProps)
)(House);
