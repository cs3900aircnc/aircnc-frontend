export const SHOW_FILTER = "SHOW_FILTER";
export const HIDE_FILTER = "HIDE_FILTER";
export const SETTING_NAV = "SETTING_NAV";

export const checkURL = url => dispatch => {
  if (url === "/" || url.includes("search")) {
    dispatch({ type: SHOW_FILTER });
  } else if (
    url.includes("myprofile") ||
    url.includes("myhouse") ||
    url.includes("mybooking") ||
    url.includes("updatehouse")
  ) {
    dispatch({ type: SETTING_NAV });
  } else {
    dispatch({ type: HIDE_FILTER });
  }
};
