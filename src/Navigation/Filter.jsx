import React, { Component } from "react";
import {
  Menu,
  Icon,
  Button,
  Popover,
  Slider,
  DatePicker,
  Dropdown
} from "antd";
import moment from "moment";
import { withRouter } from "react-router-dom";
import qs from "querystringify";
import _ from "lodash";
import $ from "jquery";

const { RangePicker } = DatePicker;

const disabledDate = current => {
  return (
    current &&
    current <
      moment()
        .endOf("day")
        .add(-1, "days")
  );
};

let confirmDate = [moment(), moment().add(1, "days")];
let confirmPriceRange = [0, 2000];
let pressOk = false;

class Filter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      guests: 1,
      adults: 1,
      children: 0,
      infants: 0,
      priceRange: [0, 2000],
      sortBy: "Default",
      showDate: [moment(), moment().add(1, "days")],
      beds: 1,
      baths: 1,
      rating: 1,
      guestsPopoverVisibility: false,
      sliderVisibility: false,
      moreFilterPopoverVisibility: false
    };
  }

  componentDidMount() {
    const query = qs.parse(this.props.location.search);
    if (query.ordering) {
      if (query.ordering === "price")
        this.setState({ sortBy: "Price low-high" });
      if (query.ordering === "-price")
        this.setState({ sortBy: "Price high-low" });
    }
    if (query.guests__gte) {
      const { guests__gte, adults, children, infants } = query;
      this.setState({
        guests: Number(guests__gte),
        adults: Number(adults),
        children: Number(children),
        infants: Number(infants)
      });
    }

    if (query.startDate) {
      const current = [
        moment(query.startDate, "YYYY-MM-DD"),
        moment(query.endDate, "YYYY-MM-DD")
      ];
      this.setState({ showDate: current });
      confirmDate = current;
    }

    if (query.price__gte && query.price__lte) {
      const range = [Number(query.price__gte), Number(query.price__lte)];
      this.setState({ priceRange: range });
      confirmPriceRange = range;
    }

    if (query.beds__gte) {
      this.setState({
        beds: Number(query.beds__gte),
        baths: Number(query.baths__gte),
        rating: Number(query.rating__gte)
      });
    }
  }

  priceRange = () => {
    const { priceRange } = this.state;
    return (
      <div className="price-range">
        Currency: AUD
        <Slider
          range
          value={priceRange}
          max={2000}
          onChange={price => this.setState({ priceRange: price })}
        />
        <div style={{ display: "flex", marginTop: 15, marginBottom: 5 }}>
          <div style={{ display: "flex", flex: 1, alignItems: "center" }}>
            <div>${priceRange[0]} AUD</div>
            <div style={{ marginLeft: 5, marginRight: 5 }}>-</div>
            <div>${priceRange[1]} AUD</div>
          </div>
          <Button
            size="small"
            onClick={() => {
              this.setState({ priceRange: [0, 2000] });
              confirmPriceRange = priceRange;
            }}
          >
            Reset
          </Button>
          <Button
            style={{ marginLeft: 15 }}
            type="primary"
            size="small"
            onClick={() => {
              this.setState({ sliderVisibility: false });
              const query = qs.parse(this.props.location.search);
              this.props.history.push({
                pathname: this.props.location.pathname,
                search: qs.stringify({
                  ...query,
                  price__gte: priceRange[0],
                  price__lte: priceRange[1]
                })
              });
              this.props.history.go(0);
              confirmPriceRange = priceRange;
              window.scrollTo(0, 0);
            }}
          >
            Apply
          </Button>
        </div>
      </div>
    );
  };

  guestsPopover = name => {
    const { guests } = this.state;
    const currentProp = name.toLowerCase();
    const current = this.state[currentProp];
    const marginTop = name === "Adults" ? 10 : 25;
    return (
      <div>
        <div
          style={{
            width: 130,
            display: "inline-block",
            fontSize: 16,
            marginTop
          }}
        >
          {name}
        </div>
        <Button
          className="minus"
          type="primary"
          shape="circle"
          icon="minus"
          ghost
          onClick={() =>
            this.setState({ [currentProp]: current - 1, guests: guests - 1 })
          }
          disabled={
            guests <= 1 || name === "Adults" ? current <= 1 : current <= 0
          }
        />
        <span
          style={{ textAlign: "center", width: 26, display: "inline-block" }}
        >
          {current}
        </span>

        <Button
          className="plus"
          type="primary"
          shape="circle"
          icon="plus"
          ghost
          onClick={() =>
            this.setState({ [currentProp]: current + 1, guests: guests + 1 })
          }
          disabled={guests >= 16}
        />
      </div>
    );
  };

  moreFilterPopover = name => {
    const currentProp = name.toLowerCase();
    const current = this.state[currentProp];
    const marginTop = name === "Rooms" ? 10 : 25;
    return (
      <div>
        <div
          style={{
            width: 130,
            display: "inline-block",
            fontSize: 16,
            marginTop
          }}
        >
          {name}
        </div>
        <Button
          className="minus"
          type="primary"
          shape="circle"
          icon="minus"
          ghost
          onClick={() => this.setState({ [currentProp]: current - 1 })}
          disabled={current === 1}
        />
        <span
          style={{ textAlign: "center", width: 26, display: "inline-block" }}
        >
          &#8805;
          {current}
        </span>

        <Button
          className="plus"
          type="primary"
          shape="circle"
          icon="plus"
          ghost
          onClick={() => this.setState({ [currentProp]: current + 1 })}
          disabled={name === "Rating" ? current >= 5 : current >= 10}
        />
      </div>
    );
  };

  resetSorter = sorter => {
    const currentSorter = this.state.sortBy;
    const shouldPush = currentSorter !== sorter;

    this.setState({ sortBy: sorter });

    let query = qs.parse(this.props.location.search);
    if (sorter === "Default") query = _.omit(query, "ordering");
    else if (sorter === "Price low-high")
      query = { ...query, ordering: "price" };
    else query = { ...query, ordering: "-price" };

    if (shouldPush) {
      this.props.history.push({
        pathname: this.props.location.pathname,
        search: qs.stringify(query)
      });
      this.props.history.go(0);
      window.scrollTo(0, 0);
    } else {
      $("main").removeClass("blur");
    }
  };

  render() {
    const {
      infants,
      adults,
      children,
      priceRange,
      sortBy,
      guests,
      beds,
      baths,
      rating,
      showDate,
      guestsPopoverVisibility,
      sliderVisibility,
      moreFilterPopoverVisibility
    } = this.state;

    return (
      <div>
        <Menu mode="horizontal">
          <Menu.Item key="guests" style={{ marginLeft: 36 }}>
            <Popover
              placement="bottomRight"
              visible={guestsPopoverVisibility}
              onVisibleChange={status => {
                this.setState({ guestsPopoverVisibility: status });
                if (status) {
                  $("main").addClass("blur");
                } else {
                  $("main").removeClass("blur");
                }
              }}
              content={
                <div style={{ marginBottom: 15 }}>
                  {this.guestsPopover("Adults")}
                  {this.guestsPopover("Children")}
                  {this.guestsPopover("Infants")}
                  <div style={{ marginTop: 25, display: "flex" }}>
                    <div style={{ flex: 1 }} />
                    <Button
                      size="small"
                      onClick={() =>
                        this.setState({
                          guests: 1,
                          adults: 1,
                          children: 0,
                          infants: 0
                        })
                      }
                    >
                      Clear
                    </Button>
                    <Button
                      style={{ marginLeft: 18 }}
                      size="small"
                      type="primary"
                      onClick={() => {
                        const query = qs.parse(this.props.location.search);
                        this.setState({ guestsPopoverVisibility: false });
                        this.props.history.push({
                          pathname: this.props.location.pathname,
                          search: qs.stringify({
                            ...query,
                            infants,
                            adults,
                            children,
                            guests__gte: guests
                          })
                        });
                        this.props.history.go(0);
                        window.scrollTo(0, 0);
                      }}
                    >
                      Apply
                    </Button>
                  </div>
                </div>
              }
              trigger="click"
            >
              <a className="ant-dropdown-link" hred="">
                {guests} Guests <Icon type="down" />
              </a>
            </Popover>
          </Menu.Item>

          <Menu.Item key="date" style={{ paddingLeft: 0 }}>
            <div className="filer-rangePicker">
              <RangePicker
                dropdownClassName="filter_range_picker_dropdown"
                disabledDate={disabledDate}
                value={showDate}
                showTime
                onOk={value => {
                  pressOk = true;
                  confirmDate = value;
                  const startDate = value[0].format("YYYY-MM-DD");
                  const endDate = value[1].format("YYYY-MM-DD");
                  const query = qs.parse(this.props.location.search);
                  this.props.history.push({
                    pathname: this.props.location.pathname,
                    search: qs.stringify({ ...query, startDate, endDate })
                  });
                  this.props.history.go(0);
                  window.scrollTo(0, 0);
                }}
                format="ll"
                onChange={value => this.setState({ showDate: value })}
                onOpenChange={status => {
                  if (!pressOk) {
                    if (!status) {
                      $("main").removeClass("blur");
                      this.setState({ showDate: confirmDate });
                    } else {
                      $("main").addClass("blur");
                    }
                  }
                }}
                allowClear={false}
              />
            </div>
          </Menu.Item>

          <Menu.Item key="price">
            <Popover
              placement="bottom"
              content={this.priceRange()}
              trigger="click"
              visible={sliderVisibility}
              onVisibleChange={status => {
                this.setState({ sliderVisibility: status });
                if (status) {
                  $("main").addClass("blur");
                } else {
                  $("main").removeClass("blur");
                }
                if (!_.isEqual(priceRange, confirmPriceRange) && !status) {
                  const query = qs.parse(this.props.location.search);
                  this.props.history.push({
                    pathname: this.props.location.pathname,
                    search: qs.stringify({
                      ...query,
                      price__gte: priceRange[0],
                      price__lte: priceRange[1]
                    })
                  });
                  this.props.history.go(0);
                  window.scrollTo(0, 0);
                  confirmPriceRange = priceRange;
                }
              }}
            >
              <a className="ant-dropdown-link" hred="">
                Price Range:
                <span style={{ marginLeft: 10, marginRight: 10 }}>
                  ${priceRange[0]} - ${priceRange[1]}
                </span>
                <Icon type="down" />
              </a>
            </Popover>
          </Menu.Item>

          <Menu.Item key="sort">
            <Dropdown
              overlay={
                <Menu>
                  <Menu.Item
                    style={{ paddingTop: 10, paddingRight: 100 }}
                    onClick={() => this.resetSorter("Default")}
                    key="sortDefault"
                  >
                    Default
                  </Menu.Item>
                  <Menu.Item
                    style={{ paddingTop: 10 }}
                    onClick={() => this.resetSorter("Price low-high")}
                    key="Price low-high"
                  >
                    Price low-high
                  </Menu.Item>
                  <Menu.Item
                    style={{ paddingTop: 10 }}
                    onClick={() => this.resetSorter("Price high-low")}
                    key="Price high-low"
                  >
                    Price high-low
                  </Menu.Item>
                </Menu>
              }
              trigger={["click"]}
              placement="bottomCenter"
              onVisibleChange={status => {
                if (status) {
                  $("main").addClass("blur");
                } else {
                  $("main").removeClass("blur");
                }
              }}
            >
              <a className="ant-dropdown-link" hred="">
                Sort by: {sortBy} <Icon type="down" />
              </a>
            </Dropdown>
          </Menu.Item>

          <Menu.Item key="moreFilters">
            <Popover
              placement="bottom"
              visible={moreFilterPopoverVisibility}
              onVisibleChange={status => {
                this.setState({ moreFilterPopoverVisibility: status });
                if (status) {
                  $("main").addClass("blur");
                } else {
                  $("main").removeClass("blur");
                }
              }}
              content={
                <div style={{ marginBottom: 15 }}>
                  {this.moreFilterPopover("Beds")}
                  {this.moreFilterPopover("Baths")}
                  {this.moreFilterPopover("Rating")}
                  <div style={{ marginTop: 25, display: "flex" }}>
                    <div style={{ flex: 1 }} />
                    <Button
                      size="small"
                      onClick={() =>
                        this.setState({ beds: 1, baths: 1, rating: 1 })
                      }
                    >
                      Clear
                    </Button>
                    <Button
                      style={{ marginLeft: 18 }}
                      size="small"
                      type="primary"
                      onClick={() => {
                        this.setState({ moreFilterPopoverVisibility: false });
                        const query = qs.parse(this.props.location.search);
                        this.props.history.push({
                          pathname: this.props.location.pathname,
                          search: qs.stringify({
                            ...query,
                            beds__gte: beds,
                            baths__gte: baths,
                            rating__gte: rating
                          })
                        });
                        this.props.history.go(0);
                        window.scrollTo(0, 0);
                      }}
                    >
                      Apply
                    </Button>
                  </div>
                </div>
              }
              trigger="click"
            >
              <a className="ant-dropdown-link" hred="">
                More Filters <Icon type="down" />
              </a>
            </Popover>
          </Menu.Item>
        </Menu>
      </div>
    );
  }
}

export default withRouter(Filter);
