import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import _ from "lodash";
import * as authAction from "../Auth/LoginAction";
import { reset } from "../Search/SearchAction";
import qs from "querystringify";
import { Input, Avatar, Dropdown, Menu, AutoComplete } from "antd";
import moment from "moment";

import Signin from "../Sign/SignIn";
import Signup from "../Sign/SignUp";
import Filter from "./Filter";
import axios from "axios";

const Search = Input.Search;

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = { term: "", dataSource: [] };
    this.actions = bindActionCreators(
      { ...authAction, reset },
      this.props.dispatch
    );
  }

  componentDidMount() {
    this.actions.autoLogin();
    const params = qs.parse(this.props.location.search);
    this.setState({ term: params.search });
  }

  handleSubmit = term => {
    const query = {
      search: term.trim(),
      startDate: moment().format("YYYY-MM-DD"),
      endDate: moment()
        .add(1, "days")
        .format("YYYY-MM-DD")
    };
    this.props.history.push({
      pathname: "/search/housings",
      search: qs.stringify(query)
    });
    window.scrollTo(0, 0);
    this.props.history.go(0);
  };

  jumpSetting = url => {
    this.props.history.push(url);
  };

  userDropdown = () => {
    return (
      <Menu>
        <Menu.Item style={{ paddingRight: 50, paddingTop: 10 }}>
          <a onClick={() => this.jumpSetting("/myprofile")}>Edit Profile</a>
        </Menu.Item>
        <Menu.Item style={{ paddingTop: 10 }}>
          <a onClick={() => this.jumpSetting("/myhouse")}>My House</a>
        </Menu.Item>
        <Menu.Item style={{ paddingTop: 10 }}>
          <a onClick={() => this.jumpSetting("/mybooking/guest")}>My Booking</a>
        </Menu.Item>
        <Menu.Item style={{ paddingTop: 10 }}>
          <a
            onClick={() => {
              this.actions.logout();
              this.props.history.push("/");
              this.props.history.go(0);
              window.scrollTo(0, 0);
            }}
          >
            Log Out
          </a>
        </Menu.Item>
      </Menu>
    );
  };

  hasLogin = () => {
    const { name, avatarurl } = this.props;
    return (
      <div
        className="collapse navbar-collapse justify-content-end"
        id="navbarTogglerDemo02"
      >
        <ul className="navbar-nav my-2 my-lg-0 mr-4">
          <li className="nav-item mr-2">
            <a className="nav-link not-active">{name} </a>
          </li>

          <li className="nav-item mr-4">
            <a className="nav-link" style={{ padding: 0, paddingLeft: 10 }}>
              <Dropdown overlay={this.userDropdown()} placement="bottomLeft">
                <Avatar icon="user" src={avatarurl} />
              </Dropdown>
            </a>
          </li>
        </ul>
      </div>
    );
  };

  unLogin = () => {
    return (
      <div
        className="collapse navbar-collapse justify-content-end"
        id="navbarTogglerDemo02"
      >
        <ul className="navbar-nav my-2 my-lg-0 mr-2">
          <li className="nav-item mr-2">
            <a
              className="nav-link"
              name="registerVisible"
              onClick={() => this.actions.changeSignUpModal(true)}
            >
              Sign up
            </a>
          </li>

          <Signup />

          <li className="nav-item mr-5">
            <a
              className="nav-link"
              onClick={() => this.actions.changeSigninModal(true)}
              name="loginVisible"
            >
              Log in
            </a>
          </li>

          <Signin />
        </ul>
      </div>
    );
  };

  autoComplete = searctTerm => {
    this.setState({ term: searctTerm });
    if (searctTerm.trim() !== "") {
      axios
        .get(`/search/autocomplete/?search=${searctTerm}`)
        .then(response => {
          const dataSource = response.data.map(word => word.neighborhood);
          // dataSource.unshift(searctTerm);
          this.setState({ dataSource });
        })
        .catch(err => console.log(err));
    } else {
      this.setState({ dataSource: [] });
    }
  };

  render() {
    const { login, loading, navStyle, filter, fixSearch } = this.props;
    const { term, dataSource } = this.state;
    return (
      <div className={fixSearch}>
        <nav
          className={`navbar navbar-expand-lg navbar-light`}
          style={navStyle}
        >
          <a
            className="navbar-brand"
            onClick={() => {
              this.props.history.push("/");
              this.actions.reset();
              window.scrollTo(0, 0);
            }}
            style={{ paddingLeft: 42 }}
          >
            Aircnc
          </a>

          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarTogglerDemo02"
            aria-controls="navbarTogglerDemo02"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>

          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            <AutoComplete
              className="search-bar"
              placeholder="Search"
              onSearch={this.autoComplete}
              value={term}
              defaultActiveFirstOption={false}
              dataSource={dataSource}
              onSelect={value => {
                if (term.trim() !== "") {
                  this.setState({ term: value });
                  this.handleSubmit(value);
                } else {
                  this.handleSubmit("");
                }
              }}
            >
              <Search onSearch={value => this.handleSubmit(value)} />
            </AutoComplete>
          </div>
          {!loading && login && this.hasLogin()}
          {!loading && !login && this.unLogin()}
        </nav>

        {filter && <Filter />}
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { login, loading, error, name, avatarurl } = state.auth;
  const { navStyle, filter, fixSearch } = state.navStyle;
  return {
    login,
    loading,
    error,
    name,
    navStyle,
    filter,
    fixSearch,
    avatarurl
  };
};

export default _.flow(
  withRouter,
  connect(mapStateToProps)
)(NavBar);
