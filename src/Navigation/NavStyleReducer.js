import { SHOW_FILTER, HIDE_FILTER, SETTING_NAV } from './NavStyleAction';

export default (
  state = {
    navStyle: {
      background: "white",
      border: ""
    },
    filter: false,
    fixSearch: 'fixed-top'
  },
  action
) => {
  switch (action.type) {
    case SHOW_FILTER: {
      return {
        navStyle: {
          background: "white",
          border: ""
        },
        filter: true,
        fixSearch: 'fixed-top'
      };
    }
    case HIDE_FILTER: {
      return {
        navStyle: {
          background: "white",
          border: ""
        },
        filter: false,
        fixSearch: 'fixed-top'
      };
    }
    case SETTING_NAV: {
      return {
        navStyle: {
          background: "white",
          border: ""
        },
        filter: false,
        fixSearch: ''
      }
    }
    default:
      return state;
  }
};
