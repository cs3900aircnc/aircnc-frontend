import firebase from "firebase/app";
import "firebase/storage";

var config = {
  apiKey: "AIzaSyCRi1OKtm8aqRKDus5yuuv0Wq6N2Q_N0do",
  authDomain: "aircnc-ac643.firebaseapp.com",
  databaseURL: "https://aircnc-ac643.firebaseio.com",
  projectId: "aircnc-ac643",
  storageBucket: "aircnc-ac643.appspot.com",
  messagingSenderId: "322018439287"
};
firebase.initializeApp(config);

const storage = firebase.storage();

export { storage, firebase as default };
