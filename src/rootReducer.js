import { combineReducers } from 'redux';

import LoginReducer from './Auth/LoginReducer';
import NavStyleReducer from './Navigation/NavStyleReducer';
import SearchReducer from './Search/SearchReducer';
import MyProfileReducer from './Profile/MyProfileReducer';

const rootReducer = combineReducers({
  auth: LoginReducer,
  navStyle: NavStyleReducer,
  search: SearchReducer,
  profile: MyProfileReducer
});

export default rootReducer;
