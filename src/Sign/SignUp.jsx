import React, { Component } from "react";
import { Modal, Icon, Input, Form, Button, Divider } from "antd";
import _ from "lodash";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as authAction from "../Auth/LoginAction";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";

const FormItem = Form.Item;

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      error: null
    };

    this.actions = bindActionCreators(authAction, this.props.dispatch);
  }

  handleOk = e => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, payload) => {
      if (err) return;

      this.setState({ loading: true });

      const { email, username, password } = payload;
      const newUser = { email, password, username };

      const loadStatus = status => {
        this.setState({ loading: status });
      };
      const errorMessage = message => {
        this.setState({ error: message });
      };

      this.actions.signUp(newUser, errorMessage, loadStatus);
    });
  };

  handleCancel = () => {
    const { form } = this.props;
    form.resetFields();
    this.actions.changeSignUpModal(false);
  };

  handleSwitch = e => {
    e.preventDefault();
    const { form } = this.props;
    form.resetFields();
    this.actions.changeSignUpModal(false);
    this.actions.changeSigninModal(true);
  };

  responseFacebook = response => {
    const { accessToken } = response;
    if (accessToken) {
      this.actions.facebookLogin(accessToken);  
    }
  };

  render() {
    const { signUpModal } = this.props;
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const { loading, error } = this.state;
    return (
      <Modal
        title="Sign up"
        visible={signUpModal}
        onCancel={this.handleCancel}
        width={400}
        footer={null}
        centered
      >
        <FacebookLogin
          appId="2083447981907761"
          autoLoad={false}
          fields="name,email,picture"
          callback={this.responseFacebook}
          icon="fa-facebook"
          render={renderProps => (
            <Button onClick={renderProps.onClick} block>
              <Icon type="facebook" theme="outlined" /> Sign up with Facebooks
            </Button>
          )}
        />

        <Divider> or </Divider>
        <Form onSubmit={this.handleOk}>
          <FormItem>
            {getFieldDecorator("username", {
              rules: [{ required: true, message: "Username is required" }]
            })(<Input prefix={<Icon type="user" />} placeholder="username" />)}
          </FormItem>
          <FormItem>
            {getFieldDecorator("email", {
              validateTrigger: "onBlur",
              rules: [
                { required: true, message: "Please input your email !" },
                { type: "email", message: "The input is not valid E-mail!" }
              ]
            })(
              <Input
                prefix={
                  <Icon type="mail" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder="example@example.com"
              />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator("password", {
              validateTrigger: "onBlur",
              rules: [
                { required: true, message: "Please input your Password !" }
              ]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                }
                type="password"
                placeholder="Password"
              />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator("confirmPassword", {
              validateTrigger: "onBlur",
              rules: [
                getFieldValue("password")
                  ? {
                      type: "enum",
                      enum: [getFieldValue("password")],
                      message: "Password does not match"
                    }
                  : {},
                {
                  required: true,
                  message: "Password confirmation is required"
                }
              ]
            })(
              <Input
                prefix={<Icon type="lock" />}
                type="password"
                placeholder="Confirm password"
              />
            )}
          </FormItem>
          {error && (
            <div style={{ color: "red" }}>
              The username has already been used !
            </div>
          )}
          Already have a account?
          <a href="" onClick={this.handleSwitch}>
            Log in!
          </a>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
            style={{ marginTop: 18 }}
            loading={loading}
            block
          >
            Sign up
          </Button>
        </Form>
      </Modal>
    );
  }
}

const mapPropsToState = state => {
  const { signUpModal } = state.auth;
  return { signUpModal };
};

export default _.flow(
  connect(mapPropsToState),
  Form.create()
)(SignUp);
