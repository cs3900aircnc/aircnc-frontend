import React, { Component } from "react";
import { Modal, Icon, Input, Form, Button, Divider } from "antd";
import _ from "lodash";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as authAction from "../Auth/LoginAction";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";

const FormItem = Form.Item;

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      loading: false
    };

    this.actions = bindActionCreators(authAction, this.props.dispatch);
  }

  handleOk = e => {
    e.preventDefault();
    const { form } = this.props;

    const loadStatus = status => {
      this.setState({ loading: status });
    };
    const errorMessage = message => {
      this.setState({ error: message });
    };

    form.validateFields((err, payload) => {
      if (err) return;
      this.setState({ loading: true });
      const { username, password } = payload;
      this.actions.login(username, password, errorMessage, loadStatus);
    });
  };

  handleCancel = () => {
    const { form } = this.props;
    this.setState({ error: null });
    form.resetFields();
    this.actions.changeSigninModal(false);
  };

  handleSignUp = e => {
    e.preventDefault();
    const { form } = this.props;
    form.resetFields();
    this.actions.changeSigninModal(false);
    this.actions.changeSignUpModal(true);
  };

  responseFacebook = response => {
    const { accessToken } = response;
    if (accessToken) {
      this.actions.facebookLogin(accessToken);  
    }
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { signinModal } = this.props;
    const { loading, error } = this.state;
    return (
      <div>
        <Modal
          title="Log in"
          visible={signinModal}
          onCancel={this.handleCancel}
          width={400}
          footer={null}
          centered
        >
          <FacebookLogin
            appId="2083447981907761"
            autoLoad={false}
            fields="name,email,picture"
            callback={this.responseFacebook}
            icon="fa-facebook"
            render={renderProps => (
              <Button onClick={renderProps.onClick} block>
                <Icon type="facebook" theme="outlined" /> Login with Facebooks
              </Button>
            )}
          />

          <Divider> or </Divider>

          <Form onSubmit={this.handleOk} className="login-form">
            <FormItem>
              {getFieldDecorator("username", {
                validateTrigger: "onBlur",
                rules: [
                  { required: true, message: "Please enter your username!" }
                ]
              })(
                <Input
                  prefix={
                    <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  placeholder="username"
                />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator("password", {
                validateTrigger: "onBlur",
                rules: [
                  { required: true, message: "Please input your Password !" }
                ]
              })(
                <Input
                  prefix={
                    <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  type="password"
                  placeholder="Password"
                />
              )}
            </FormItem>
            {error && (
              <div style={{ color: "red", marginBottom: 10 }}>
                Incorrect username or password !
              </div>
            )}
            Don't have a account?
            <a href="" onClick={this.handleSignUp}>
              register now !
            </a>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
              style={{ marginTop: 15 }}
              loading={loading}
              block
            >
              Log in
            </Button>
          </Form>
        </Modal>
      </div>
    );
  }
}

const mapPropsToState = state => {
  const { signinModal } = state.auth;
  return { signinModal };
};

export default _.flow(
  connect(mapPropsToState),
  Form.create()
)(SignIn);
