import React from "react";
import { Menu, Icon, BackTop } from "antd";
import { connect } from "react-redux";
import _ from "lodash";
import { bindActionCreators } from "redux";
import * as checkURL from "../Navigation/NavStyleAction";
import { withRouter } from "react-router-dom";

import MyProfile from "./MyProfile";
import MyHouse from "./MyHouse";
import MyBooking from "./MyBooking";
import UpdateHouse from "./UpdateHouse";

class ProfileNavigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activateKey: [""]
    };
    this.actions = bindActionCreators(checkURL, this.props.dispatch);
  }

  componentWillReceiveProps(nextprops) {
    const currentPath = nextprops.location.pathname;
    this.setkey(currentPath);
  }

  componentDidMount() {
    
    const currentPath = this.props.location.pathname;
    this.actions.checkURL(currentPath);
    this.setkey(currentPath);
  }

  setkey = currentPath => {
    const activateKey = currentPath.substring(1);

    if (
      currentPath === "/mybooking/guest" ||
      currentPath === "/mybooking/host"
    ) {
      this.setState({ activateKey: ["mybooking"] });
    } else if (currentPath.substring(1, 12) === "updatehouse") {
      this.setState({ activateKey: ["myhouse"] });
    } else {
      this.setState({ activateKey: [activateKey] });
    }
  };

  render() {
    const { activateKey } = this.state;
    const currentPath = this.props.location.pathname.substring(1);
    const { login, loading } = this.props;

    if (!login && !loading) {
      this.props.history.push("/");
    }

    return (
      <div
        className="row justify-content-center"
        style={{ marginTop: 100, marginBottom: 50 }}
      >
        <div className="col-2">
          <Menu
            style={{ width: 200 }}
            selectedKeys={activateKey}
            mode="inline"
            onClick={item => {
              const { key, keyPath } = item;
              if (key === "mybooking") {
                this.props.history.push("/mybooking/guest");
                this.setState({ activateKey: ["mybooking"] });
              } else {
                this.props.history.push(`/${key}`);
                this.setState({ activateKey: keyPath });
              }
            }}
          >
            <Menu.Item key="myprofile">
              <Icon type="user" />
              <span>My Profile</span>
            </Menu.Item>
            <Menu.Item key="myhouse">
              <Icon type="home" />
              <span>My House</span>
            </Menu.Item>
            <Menu.Item key="mybooking">
              <Icon type="calendar" />
              <span>My Booking</span>
            </Menu.Item>
          </Menu>
        </div>
        <div className="col-7">
          {currentPath === "myprofile" && <MyProfile />}
          {currentPath === "myhouse" && <MyHouse />}
          {currentPath === "mybooking/host" && <MyBooking />}
          {currentPath === "mybooking/guest" && <MyBooking />}
          {currentPath.substring(0, 11) === "updatehouse" && <UpdateHouse />}
        </div>
        <BackTop />
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { login, loading } = state.auth;
  return { login, loading };
};

export default _.flow(
  withRouter,
  connect(mapStateToProps)
)(ProfileNavigation);
