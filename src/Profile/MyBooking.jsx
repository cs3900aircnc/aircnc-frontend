import React, { Component } from "react";
import { Card, List, Button, Modal, Rate, message } from "antd";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { fetchBooking, confirmOrder } from "./MyProfileAction";
import _ from "lodash";
import moment from "moment";
import { withRouter } from "react-router-dom";

const confirm = Modal.confirm;

const truncate = s => {
  return s.length >= 85 ? s.slice(0, 85) + " ..." : s;
};

const tabList = [
  {
    key: "As Guest",
    tab: "As Guest"
  },
  {
    key: "As Host",
    tab: "As Host"
  }
];

const bookingStatus = s => {
  switch (s) {
    case "P":
      return "Pending";
    case "A":
      return "Approved";
    case "C":
      return "Complete";
    case "R":
      return "Reject";
    default:
      break;
  }
};

class MyBooking extends Component {
  constructor(props) {
    super(props);

    this.state = {
      key: "As Guest",
      messageVisibility: false,
      currentBooking: null,
      approveLoading: false,
      rejectLoading: false,
      completeLoading: false,
      ratingList: {},
      rateLoaing: false
    };

    this.actions = bindActionCreators(
      { fetchBooking, confirmOrder },
      this.props.dispatch
    );
  }

  componentDidMount() {
    this.actions.fetchBooking();
    const path = this.props.location.pathname;
    if (path === "/mybooking/host") {
      this.setState({ key: "As Host" });
    } else {
      this.setState({ key: "As Guest" });
    }
  }

  render() {
    const {
      key,
      messageVisibility,
      currentBooking,
      approveLoading,
      rejectLoading,
      completeLoading,
      ratingList,
      rateLoaing
    } = this.state;

    const data =
      key === "As Guest" ? this.props.guestInfo : this.props.hostInfo;

    const startLoading = loadingItem => this.setState({ [loadingItem]: true });
    const endLoaing = loadingItem => {
      this.setState({ [loadingItem]: false });
      this.props.history.go(0);
    };
    const confirmOrder = (pk, action, finish) => {
      this.actions.confirmOrder(pk, action, finish);
    };

    return (
      <Card
        style={{ width: 700 }}
        title="My Booking"
        tabList={tabList}
        activeTabKey={this.state.key}
        onTabChange={key => {
          if (key === "As Host") {
            this.props.history.push("/mybooking/host");
          } else {
            console.log(key);
            this.props.history.push("/mybooking/guest");
          }
        }}
      >
        <Modal
          visible={messageVisibility}
          onCancel={() => this.setState({ messageVisibility: false })}
          footer={null}
          closable={false}
        >
          <i>
            Message from {currentBooking ? currentBooking.guest.username : null}
          </i>
          :<br /> <br />
          {currentBooking ? currentBooking.message : null}
        </Modal>

        <List
          itemLayout="horizontal"
          dataSource={data}
          renderItem={item => (
            <List.Item>
              <List.Item.Meta
                avatar={
                  <img
                    style={{ width: 200, height: 140, cursor: "pointer" }}
                    alt="house img"
                    src={item.house.images[0]}
                    onClick={() =>
                      this.props.history.push(`/house/${item.house.pk}`)
                    }
                  />
                }
                title={
                  <div
                    onClick={() =>
                      this.props.history.push(`/house/${item.house.pk}`)
                    }
                    style={{ font: 25, cursor: "pointer" }}
                  >
                    {truncate(item.house.listing_name)}
                  </div>
                }
                description={
                  <div>
                    <div style={{ marginBottom: 20 }}>
                      {moment(item.startDate).format("ll")} -{" "}
                      {moment(item.endDate).format("ll")}
                    </div>
                    <div style={{ display: "flex" }}>
                      <div style={{ flex: 1, font: 25 }}>
                        Status : {bookingStatus(item.status)} <br />
                        {key === "As Host" && (
                          <a
                            style={{ color: "#1890ff" }}
                            onClick={() => {
                              this.setState({
                                messageVisibility: true,
                                currentBooking: item
                              });
                            }}
                          >
                            view guest message
                          </a>
                        )}
                      </div>
                      {item.status === "C" &&
                        !item.rating &&
                        key === "As Guest" && (
                          <div>
                            <Rate
                              style={{ height: 5, fontSize: 12 }}
                              onChange={value => {
                                ratingList[item.pk] = value;
                                this.setState({ ratingList });
                              }}
                            />
                            <Button
                              style={{ width: 95 }}
                              disabled={rateLoaing}
                              loading={rateLoaing}
                              onClick={() => {
                                if (!(item.pk in ratingList)) {
                                  message.info("Please rate this house first");
                                } else {
                                  this.setState({ rateLoaing: true })
                                  this.actions.confirmOrder(
                                    item.pk,
                                    "rate",
                                    () => {
                                      this.setState({ rateLoaing: false })
                                      this.props.history.go(0);
                                    },
                                    ratingList[item.pk]
                                  );
                                }
                              }}
                            >
                              Rate
                            </Button>
                          </div>
                        )}
                      {item.status === "A" &&
                        key === "As Guest" && (
                          <Button
                            type="primary"
                            onClick={() => {
                              confirm({
                                title: "Are you sure to complete this booking?",
                                okText: "comlete",
                                cancelText: "Cancel",
                                maskClosable: false,
                                okButtonProps: {
                                  disabled: completeLoading,
                                  loading: completeLoading
                                },
                                cancelButtonProps: {
                                  disabled: completeLoading
                                },
                                onOk() {
                                  startLoading("completeLoading");
                                  confirmOrder(item.pk, "complete", () =>
                                    endLoaing("completeLoading")
                                  );
                                }
                              });
                            }}
                          >
                            Complete
                          </Button>
                        )}
                      {item.status === "P" &&
                        key === "As Host" && (
                          <div style={{ marginTop: 10 }}>
                            <Button
                              type="danger"
                              style={{ width: 85 }}
                              onClick={() => {
                                confirm({
                                  title: "Are you sure to reject this request?",
                                  okText: "Reject",
                                  okType: "danger",
                                  cancelText: "Cancel",
                                  maskClosable: false,
                                  okButtonProps: {
                                    disabled: rejectLoading,
                                    loading: rejectLoading
                                  },
                                  cancelButtonProps: {
                                    disabled: rejectLoading
                                  },
                                  onOk() {
                                    startLoading("rejectLoading");
                                    confirmOrder(item.pk, "reject", () =>
                                      endLoaing("rejectLoading")
                                    );
                                  }
                                });
                              }}
                            >
                              Reject
                            </Button>
                            <Button
                              style={{ width: 85, marginLeft: 15 }}
                              type="primary"
                              onClick={() => {
                                confirm({
                                  title:
                                    "Are you sure to approve this request?",
                                  okText: "Approve",
                                  cancelText: "Cancel",
                                  maskClosable: false,
                                  okButtonProps: {
                                    disabled: approveLoading,
                                    loading: approveLoading
                                  },
                                  cancelButtonProps: {
                                    disabled: approveLoading
                                  },
                                  onOk() {
                                    startLoading("approveLoading");
                                    confirmOrder(item.pk, "approve", () =>
                                      endLoaing("approveLoading")
                                    );
                                  }
                                });
                              }}
                            >
                              Approve
                            </Button>
                          </div>
                        )}
                    </div>
                  </div>
                }
              />
            </List.Item>
          )}
        />
      </Card>
    );
  }
}

const mapStateToProps = state => {
  const { guestInfo, hostInfo } = state.profile;
  return { guestInfo, hostInfo };
};

export default _.flow(
  withRouter,
  connect(mapStateToProps)
)(MyBooking);
