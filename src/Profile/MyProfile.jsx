import React from "react";
import { Form, Input, Select, Button, Card, Avatar, Upload } from "antd";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import _ from "lodash";
import { storage } from "../Firebase";
import { updateAvatar } from "../Auth/LoginAction";
import { withRouter } from "react-router-dom";
import { fetchUser, updateUser } from "./MyProfileAction";

const FormItem = Form.Item;
const Option = Select.Option;

const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 }
  }
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0
    },
    sm: {
      span: 16,
      offset: 8
    }
  }
};

class MyProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmDirty: false,
      autoCompleteResult: [],
      avatarURL: "",
      avatarLoading: false,
      saveLoading: false,
      currentFile: null
    };
    this.actions = bindActionCreators(
      { updateAvatar, fetchUser, updateUser },
      this.props.dispatch
    );
  }

  componentDidMount() {
    this.actions.fetchUser();
  }

  handleSubmit = e => {
    e.preventDefault();
    this.setState({ saveLoading: true });
    const finishSave = () => this.setState({ saveLoading: false });
    const refresh = () => this.props.history.go(0);
    this.props.form.validateFieldsAndScroll((err, payload) => {
      if (err) {
        console.log(err);
        finishSave();
        return;
      }
      const { email, firstname, phone, description } = payload;
      const userProfile = {
        email,
        phone,
        description,
        first_name: firstname
      };
      this.actions.updateUser(userProfile, finishSave, refresh);
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      avatarurl,
      username,
      email,
      firstName,
      phone,
      description
    } = this.props;

    const { avatarLoading, saveLoading } = this.state;

    const prefixSelector = getFieldDecorator("prefix", {
      initialValue: "61"
    })(
      <Select style={{ width: 70 }}>
        <Option value="61">+61</Option>
      </Select>
    );

    return (
      <Card style={{ width: 700 }} title="My Profile">
        <div style={{ marginLeft: 167, marginBottom: 25 }}>
          {avatarurl === "" ? (
            <Avatar shape="square" size={64} icon="user" />
          ) : (
            <Avatar shape="square" size={64} icon="user" src={avatarurl} />
          )}

          <Upload
            action=""
            showUploadList={false}
            disabled={avatarLoading}
            beforeUpload={file => this.setState({ currentFile: file })}
            customRequest={() => {
              const file = this.state.currentFile;
              this.setState({ avatarLoading: true });
              const finishLoading = () =>
                this.setState({ avatarLoading: false });
              const uploadTask = storage
                .ref(`${username}/${file.name}`)
                .put(file);
              uploadTask.on(
                "state_changed",
                snapshot => {},
                err => {
                  console.log(err);
                  finishLoading();
                },
                () => {
                  storage
                    .ref(`${username}`)
                    .child(`${file.name}`)
                    .getDownloadURL()
                    .then(url => {
                      this.actions.updateAvatar(url, finishLoading);
                    });
                }
              );
            }}
          >
            <Button
              style={{ marginLeft: 50 }}
              icon="upload"
              loading={avatarLoading}
            >
              Upload
            </Button>
          </Upload>
        </div>
        <Form
          onSubmit={this.handleSubmit}
          style={{ marginLeft: -50, marginRight: 50 }}
        >
          <FormItem {...formItemLayout} label="E-mail">
            {getFieldDecorator("email", {
              initialValue: email,
              rules: [
                {
                  type: "email",
                  message: "The input is not valid E-mail!"
                },
                {
                  required: true,
                  message: "Please input your E-mail!"
                }
              ]
            })(<Input disabled={true} />)}
          </FormItem>
          <FormItem {...formItemLayout} label={<span>First Name&nbsp;</span>}>
            {getFieldDecorator("firstname", {
              initialValue: firstName,
              rules: [
                {
                  required: false,
                  whitespace: true
                }
              ]
            })(<Input disabled={saveLoading} />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Phone Number">
            {getFieldDecorator("phone", {
              initialValue: phone,
              rules: []
            })(
              <Input
                addonBefore={prefixSelector}
                disabled={saveLoading}
                style={{ width: "100%" }}
              />
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Describe Yourself">
            {getFieldDecorator("description", {
              initialValue: description,
              rules: []
            })(
              <TextArea
                style={{ width: "100%" }}
                rows={6}
                placeholder="Talk about your self"
                disabled={saveLoading}
              />
            )}
          </FormItem>
          <FormItem {...tailFormItemLayout}>
            <Button
              type="primary"
              htmlType="submit"
              block
              loading={saveLoading}
            >
              Save
            </Button>
          </FormItem>
        </Form>
      </Card>
    );
  }
}

const mapPropsToState = state => {
  const { avatarurl, name } = state.auth;
  const { email, firstName, phone, description } = state.profile;
  return { avatarurl, username: name, email, firstName, phone, description };
};

export default _.flow(
  withRouter,
  Form.create(),
  connect(mapPropsToState)
)(MyProfile);
