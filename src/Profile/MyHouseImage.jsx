import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class MyHouseImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imgHeight: 210
    };
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
  }

  updateWindowDimensions = () => {
    if (window.innerWidth >= 1200) {
      this.setState({ imgHeight: (window.innerWidth / 4.7 / 3) * 2 });
    } else if (window.innerWidth >= 992) {
      this.setState({ imgHeight: (window.innerWidth / 3.7 / 3) * 2 });
    } else if (window.innerWidth >= 576) {
      this.setState({ imgHeight: (window.innerWidth / 2.7 / 3) * 2 });
    } else {
      this.setState({ imgHeight: ((window.innerWidth - 120) / 3) * 2 });
    }
  };

  renderImage = (images, pk) => {
    const { imgHeight } = this.state;
    const { changeHouseInfo } = this.props;
    return (
      <div>
        {images.map((img, index) => {
          if (index === 0) {
            return (
              <div className="carousel-item active" key={index}>
                <img
                  className="d-block w-100"
                  src={img}
                  alt={`${index} slide`}
                  style={{ height: imgHeight }}
                  onClick={() => changeHouseInfo(pk)}
                />
              </div>
            );
          } else {
            return (
              <div className="carousel-item" key={index}>
                <img
                  className="d-block w-100"
                  style={{ height: imgHeight }}
                  src={img}
                  alt={`${index} slide`}
                  onClick={() => changeHouseInfo(pk)}
                />
              </div>
            );
          }
        })}
      </div>
    );
  };

  render() {
    const { pk, images } = this.props;
    const id = `HouseImage${pk}`;
    const href = `#HouseImage${pk}`;

    return (
      <div id={id} className="carousel slide" data-interval="false">
        {images && (
          <div className="carousel-inner">{this.renderImage(images, pk)}</div>
        )}
        <a
          className="carousel-control-prev"
          href={href}
          role="button"
          data-slide="prev"
        >
          <span className="carousel-control-prev-icon" aria-hidden="true" />
          <span className="sr-only">Previous</span>
        </a>
        <a
          className="carousel-control-next"
          href={href}
          role="button"
          data-slide="next"
        >
          <span className="carousel-control-next-icon" aria-hidden="true" />
          <span className="sr-only">Next</span>
        </a>
      </div>
    );
  }
}

export default withRouter(MyHouseImage);
