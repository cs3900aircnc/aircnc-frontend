import React, { Component } from "react";
import {
  Card,
  Form,
  Input,
  Select,
  Button,
  InputNumber,
  DatePicker,
  Icon,
  Upload,
  Modal,
  message,
  AutoComplete
} from "antd";
import _ from "lodash";
import { withRouter } from "react-router-dom";
import moment from "moment";
import axios from "axios";
import { updateHouse } from "./MyProfileAction";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

const FormItem = Form.Item;
const Option = Select.Option;
const { RangePicker } = DatePicker;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 }
  }
};

const formItemLayoutWithOutLabel = {
  wrapperCol: {
    xs: { span: 24, offset: 8 },
    sm: { span: 20, offset: 8 }
  }
};

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0
    },
    sm: {
      span: 16,
      offset: 8
    }
  }
};

const disabledDate = current => {
  return (
    current &&
    current <
      moment()
        .endOf("day")
        .add(-1, "days")
  );
};

const uploadButton = (
  <div>
    <Icon type="plus" />
    <div className="ant-upload-text">Upload</div>
  </div>
);

class PriceInput extends React.Component {
  constructor(props) {
    super(props);

    const value = props.value || {};
    this.state = {
      number: value.number || 0,
      currency: value.currency || "AUD"
    };
  }

  componentWillReceiveProps(nextProps) {
    if ("value" in nextProps) {
      const value = nextProps.value;
      this.setState(value);
    }
  }

  handleNumberChange = e => {
    const number = parseInt(e.target.value || 0, 10);
    if (isNaN(number)) {
      return;
    }
    if (!("value" in this.props)) {
      this.setState({ number });
    }
    this.triggerChange({ number });
  };

  handleCurrencyChange = currency => {
    if (!("value" in this.props)) {
      this.setState({ currency });
    }
    this.triggerChange({ currency });
  };

  triggerChange = changedValue => {
    const onChange = this.props.onChange;
    if (onChange) {
      onChange(Object.assign({}, this.state, changedValue));
    }
  };

  render() {
    const { size, disabled } = this.props;
    const state = this.state;
    return (
      <span>
        <Input
          type="text"
          size={size}
          value={state.number}
          onChange={this.handleNumberChange}
          disabled={disabled}
          style={{ width: "65%", marginRight: "3%" }}
        />
        <Select
          value={state.currency}
          size={size}
          style={{ width: "32%" }}
          onChange={this.handleCurrencyChange}
          disabled={disabled}
        >
          <Option value="AUD">AUD</Option>
        </Select>
      </span>
    );
  }
}

class UpdateHouse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      house: {},
      create: false,
      keys: [0],
      fileList: [],
      previewVisible: false,
      previewImage: "",
      loading: false,
      dataSource: []
    };

    this.actions = bindActionCreators({ updateHouse }, this.props.dispatch);
  }

  componentDidMount() {
    const { form } = this.props;
    const { id } = this.props.match.params;
    this.setState({ create: id === "create" });
    if (id === "create") return;
    axios
      .get(`/rooms/${id}/`)
      .then(response => {
        const data = response.data;
        const house = { ...data, rules: JSON.parse(data.rules) };
        this.setState({
          house,
          keys: Array.from(Array(house.rules.length).keys()),
          fileList: house.images.map((url, index) => {
            return { uid: index, url };
          })
        });
        form.setFieldsValue({
          keys: Array.from(Array(house.rules.length).keys())
        });
      })
      .catch(err => console.log(err));
  }

  checkPrice = (rule, value, callback) => {
    if (value.number > 0) {
      callback();
      return;
    }
    callback("Price must greater than zero!");
  };

  handleSubmit = e => {
    e.preventDefault();

    const { fileList } = this.state;
    if (fileList.length === 0) {
      message.info(
        " You must upload at least one image to create a new house "
      );
      return;
    }
    const { id } = this.props.match.params;

    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({ loading: true });
        const price = values.price.number;
        const rulesKey = Object.keys(values).filter(v => v.includes("rule"));
        const rules = _.pick(values, rulesKey);
        const temp = _.omit(values, rulesKey.concat(["keys", "period"]));
        const start = values.period[0]
          .endOf("day")
          .add(-1, "day")
          .format();
        const end = values.period[1].endOf("day").format();
        const startDate = start.substring(0, start.length - 6) + "Z";
        const endDate = end.substring(0, end.length - 6) + "Z";
        const updateHouse = {
          ...temp,
          user: 1000,
          price,
          rules: JSON.stringify(Object.values(rules)),
          startDate,
          endDate
        };
        const finished = () => {
          this.setState({ loading: false });
          this.props.history.push("/myhouse");
          this.props.history.go(0);
          window.scrollTo(0, 0);
        };
        this.actions.updateHouse(updateHouse, id, fileList, finished);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      create,
      house,
      keys,
      fileList,
      previewVisible,
      previewImage,
      loading,
      dataSource
    } = this.state;
    const { rules } = house;

    getFieldDecorator("keys", { initialValue: [] });

    return (
      <Card style={{ width: 700 }} title="My House">
        <Form
          onSubmit={this.handleSubmit}
          style={{ marginLeft: -50, marginRight: 50 }}
        >
          <FormItem {...formItemLayout} label="House Name">
            {getFieldDecorator("listing_name", {
              initialValue: house ? house.listing_name : "",
              rules: [
                { required: true, message: "Please input the house name!" }
              ]
            })(<Input disabled={loading} />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Address">
            {getFieldDecorator("address", {
              initialValue: house ? house.address : "",
              rules: [
                { required: true, message: "Please input the house Address!" }
              ]
            })(
              <AutoComplete
                dataSource={dataSource}
                defaultActiveFirstOption={false}
                onSearch={term => {
                  const searchTerm = term.trim();
                  if (searchTerm === "") {
                    this.setState({ dataSource: [] });
                    return;
                  }
                  axios
                    .get(`/rooms/addressautocomplete/${searchTerm}/`)
                    .then(response => {
                      this.setState({ dataSource: response.data });
                    })
                    .catch(err => console.log(err.response));
                }}
              >
                <Input disabled={loading} />
              </AutoComplete>
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Price">
            {getFieldDecorator("price", {
              initialValue: {
                number: house ? house.price : 0,
                currency: "AUD"
              },
              rules: [{ validator: this.checkPrice }, { required: true }]
            })(<PriceInput disabled={loading} />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Neighborhood">
            {getFieldDecorator("neighborhood", {
              initialValue: house ? house.neighborhood : "",
              rules: [
                { required: true, message: "Please input the neighborhood!" }
              ]
            })(<Input disabled={loading} />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Num Bathroom">
            {getFieldDecorator("baths", {
              initialValue: house ? house.baths : 1,
              rules: [
                {
                  required: true,
                  message: "Please input the number of bathroom!"
                }
              ]
            })(<InputNumber min={1} max={20} disabled={loading} />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Num Bedroom">
            {getFieldDecorator("beds", {
              initialValue: house ? house.beds : 1,
              rules: [
                {
                  required: true,
                  message: "Please input the number of Bedroom!"
                }
              ]
            })(<InputNumber min={1} max={20} disabled={loading} />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Num Guests">
            {getFieldDecorator("guests", {
              initialValue: house ? house.guests : 1,
              rules: [
                {
                  required: true,
                  message: "Please input the number of guests!"
                }
              ]
            })(<InputNumber min={1} max={20} disabled={loading} />)}
          </FormItem>

          <FormItem label={"Rules"} {...formItemLayout}>
            {getFieldDecorator(`rule0`, {
              initialValue: rules ? rules[0] : "",
              validateTrigger: ["onChange", "onBlur"],
              rules: []
            })(
              <Input
                placeholder="Rule"
                style={{ width: "75%", marginRight: 8 }}
                disabled={loading}
              />
            )}
          </FormItem>

          {keys.slice(1).map(k => (
            <FormItem {...formItemLayoutWithOutLabel} required={false} key={k}>
              {getFieldDecorator(`rule${k}`, {
                initialValue: rules ? rules[k] : "",
                validateTrigger: ["onChange", "onBlur"],
                rules: [
                  {
                    required: true,
                    whitespace: true,
                    message: "Please input a rule or delete this field."
                  }
                ]
              })(
                <Input
                  placeholder="Rule"
                  style={{ width: "60%", marginRight: 8 }}
                  disabled={loading}
                />
              )}
              <Button
                shape="circle"
                onClick={() => {
                  this.setState({ keys: keys.filter(key => key !== k) });
                }}
                disabled={loading}
                size="small"
              >
                <Icon type="minus" />
              </Button>
            </FormItem>
          ))}

          <FormItem {...formItemLayoutWithOutLabel}>
            <Button
              type="dashed"
              disabled={loading}
              onClick={() => {
                keys.push(keys[keys.length - 1] + 1);
                this.setState({ keys });
              }}
              style={{ width: "60%" }}
            >
              <Icon type="plus" /> Add field
            </Button>
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Lease Period"
            style={{ marginTop: 30 }}
          >
            {getFieldDecorator("period", {
              initialValue: house
                ? [moment(house.startDate), moment(house.endDate)]
                : null,
              rules: [
                {
                  type: "array",
                  required: true,
                  message: "Please select time!"
                }
              ]
            })(
              <RangePicker
                disabledDate={disabledDate}
                format="ll"
                disabled={!create || loading}
              />
            )}
            {house && (
              <div>
                You will be allowed to set the leasing time after{" "}
                {moment(house.endDate).format("ll")}
              </div>
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Description">
            {getFieldDecorator("listing_description", {
              initialValue: house ? house.listing_description : "",
              rules: [
                {
                  required: true,
                  message: "Please input the house description!"
                }
              ]
            })(
              <TextArea style={{ width: "100%" }} rows={6} disabled={loading} />
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Images">
            <div className="clearfix">
              <Upload
                action=""
                listType="picture-card"
                disabled={loading}
                fileList={fileList}
                beforeUpload={file => {
                  return false;
                }}
                onPreview={file => {
                  this.setState({
                    previewImage: file.url || file.thumbUrl,
                    previewVisible: true
                  });
                }}
                onChange={({ fileList }) => this.setState({ fileList })}
                multiple={false}
              >
                {fileList.length >= 6 ? null : uploadButton}
              </Upload>
              <Modal
                visible={previewVisible}
                footer={null}
                onCancel={() => this.setState({ previewVisible: false })}
              >
                <img
                  alt="example"
                  style={{ width: "100%" }}
                  src={previewImage}
                />
              </Modal>
            </div>
          </FormItem>

          <FormItem {...tailFormItemLayout}>
            <Button
              type="primary"
              htmlType="submit"
              block
              disabled={loading}
              loading={loading}
            >
              {create ? <span>Create</span> : <span>Update</span>}
            </Button>
          </FormItem>
        </Form>
      </Card>
    );
  }
}

export default _.flow(
  withRouter,
  Form.create(),
  connect()
)(UpdateHouse);
