import {
  FETCH_HOUSE,
  FETCH_USER,
  START_FETCH,
  FETCH_GUEST,
  FETCH_HOST
} from "./MyProfileAction";

export default (
  state = {
    houseData: [],
    loading: false,
    email: "",
    firstName: "",
    phone: "",
    description: "",
    guestInfo: [],
    hostInfo: []
  },
  action
) => {
  switch (action.type) {
    case START_FETCH: {
      return { ...state, loading: true };
    }
    case FETCH_HOUSE: {
      return { ...state, houseData: action.payload, loading: false };
    }
    case FETCH_USER: {
      const { email, first_name, phone, description } = action.payload;
      return { ...state, email, firstName: first_name, phone, description };
    }
    case FETCH_GUEST: {
      return { ...state, guestInfo: action.payload };
    }
    case FETCH_HOST: {
      return { ...state, hostInfo: action.payload };
    }
    default:
      return state;
  }
};
