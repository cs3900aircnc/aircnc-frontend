import axios from "axios";
import { storage } from "../Firebase";
import _ from "lodash";
import getToken from "../Auth/GetToken";

export const FETCH_HOUSE = "FETCH_HOUSE";
export const FETCH_USER = "FETCH_USER";
export const START_FETCH = "START_FETCH";
export const FETCH_GUEST = "FETCH_GUEST";
export const FETCH_HOST = "FETCH_HOST";

export const getMyhouse = () => dispatch => {
  const token = getToken();

  const onSuccess = response => {
    dispatch({ type: FETCH_HOUSE, payload: response.data });
  };

  const onFail = err => {
    console.log(err);
  };

  dispatch({ type: START_FETCH });
  axios
    .get("/rooms/my/", { headers: { Authorization: token} })
    .then(onSuccess)
    .catch(onFail);
};

export const fetchUser = () => dispatch => {
  const token = getToken();
  axios
    .get("/current_user/", { headers: { Authorization: token } })
    .then(response => {
      dispatch({ type: FETCH_USER, payload: response.data });
    })
    .catch(err => {
      console.log(err);
    });
};

export const updateUser = (user, finishSave, refresh) => dispatch => {
  const token = getToken();
  axios
    .post("/updateProfile/", user, {
      headers: { Authorization: token }
    })
    .then(response => {
      dispatch({ type: FETCH_USER, payload: response.data });
      finishSave();
      refresh();
    })
    .catch(err => {
      console.log(err);
      finishSave();
    });
};

export const updateHouse = (house, id, fileList, finished) => dispatch => {
  const token = getToken();

  const uploadPromise = fileList.map(
    antFile =>
      new Promise(resolve => {
        if (_.has(antFile, "thumbUrl")) {
          const file = antFile.originFileObj;
          const uploadTask = storage.ref(`house${id}/${file.name}`).put(file);
          uploadTask.on(
            "state_changed",
            snapshot => {},
            err => {
              console.log("error", err);
            },
            () => {
              storage
                .ref(`house${id}`)
                .child(`${file.name}`)
                .getDownloadURL()
                .then(url => {
                  resolve(url);
                });
            }
          );
        } else {
          resolve(antFile.url);
        }
      })
  );

  const uploadImage = houseid =>
    Promise.all(uploadPromise).then(image => {
      axios
        .post(
          `/rooms/my/addimage/${houseid}/`,
          { image },
          {
            headers: { Authorization: token }
          }
        )
        .then(() => finished())
        .catch(err => {
          console.log(err.response);
          finished();
        });
    });

  const postUrl =
    id === "create" ? "/rooms/create/" : `/rooms/my/update_details/${id}/`;

  axios
    .post(postUrl, house, {
      headers: { Authorization: token }
    })
    .then(response => {
      const { pk } = response.data;
      if (id === "create") {
        uploadImage(pk);
      } else {
        uploadImage(id);
      }
    })
    .catch(err => {
      console.log("error", err.response);
    });
};

export const fetchBooking = () => dispatch => {
  const token = getToken();

  axios
    .get("/bookings/asGuest/", {
      headers: { Authorization: token }
    })
    .then(response => {
      dispatch({ type: FETCH_GUEST, payload: response.data });
    })
    .catch(err => console.log(err.response));

  axios
    .get("/bookings/asHost/", {
      headers: { Authorization: token }
    })
    .then(response => {
      dispatch({ type: FETCH_HOST, payload: response.data });
    })
    .catch(err => console.log(err.response));
};

export const confirmOrder = (pk, action, finish, rating) => dispatch => {
  const token = getToken();

  const body = rating ? { rating } : {};

  axios
    .post(`/bookings/${pk}/${action}/`, body, {
      headers: { Authorization: token }
    })
    .then(response => {
      console.log(response);
      finish();
    })
    .catch(err => {
      console.log(err.response);
      finish();
    });
};
