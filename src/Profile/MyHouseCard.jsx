import React, { Component } from "react";
import { Card, Rate } from "antd";
import MyHouseImage from "./MyHouseImage";
import { withRouter } from "react-router-dom";

const truncate = s => {
  return s.length >= 45 ? s.slice(0, 45) + " ..." : s;
};

class MyHouseCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pk: null,
      name: null,
      rating: null,
      price: null,
      images: [],
      loading: true
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    const { pk, images } = this.props.house;
    const { listing_name, price, rating } = this.props.house;
    this.setState({
      pk,
      name: listing_name,
      price,
      rating,
      images,
      loading: false
    });
  }

  render() {
    const { pk, images, price, rating, name } = this.state;
    const { changeHouseInfo } = this.props;
    return (
      <div style={{ marginTop: 20, width: 300, marginLeft: 10 }}>
        <Card
          cover={<MyHouseImage pk={pk} images={images} changeHouseInfo={changeHouseInfo}/>}
          hoverable={true}
          bodyStyle={{
            paddingTop: 10,
            paddingBottom: 5,
            height: 110,
            paddingLeft: 10,
            paddingRight: 10
          }}
          bordered={false}
          style={{ borderRadius: 5 }}
          loading={this.state.loading}
        >
          {!this.state.loading && (
            <div onClick={() => changeHouseInfo(pk)}>
              <h6 style={{ overflowWrap: "break-word" }}>{truncate(name)}</h6>
              <div style={{ display: "flex" }}>
                <div style={{ flex: 1 }}>
                  <div>${price} AUD per night</div>
                  <Rate
                    disabled
                    defaultValue={Number(rating)}
                    allowHalf
                    style={{ height: 5, fontSize: 12 }}
                  />
                </div>
                Edit
              </div>
            </div>
          )}
        </Card>
      </div>
    );
  }
}

export default withRouter(MyHouseCard);
