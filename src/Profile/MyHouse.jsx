import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getMyhouse } from "./MyProfileAction";
import { Card, Button, Icon, Spin } from "antd";
import MyHouseCard from "./MyHouseCard";
import _ from "lodash";
import { withRouter } from "react-router-dom";

class MyHouse extends Component {
  constructor(props) {
    super(props);

    this.actions = bindActionCreators({ getMyhouse }, this.props.dispatch);
  }

  componentDidMount() {
    this.actions.getMyhouse();
  }

  render() {
    const { houseData, loading } = this.props;
    return (
      <Card
        style={{ width: 700, marginBottom: 50 }}
        title="My House"
        extra={
          <Button
            onClick={() => this.props.history.push("/updatehouse/create")}
          >
            <Icon type="plus" theme="outlined" />
            Create new house
          </Button>
        }
      >
        {loading && (
          <div style={{ textAlign: "center" }}>
            <Spin size="large" />
          </div>
        )}
        {!loading &&
          houseData.length !== 0 && (
            <div style={{ display: "flex", flexWrap: "wrap" }}>
              {houseData.map(house => (
                <MyHouseCard
                  key={house.pk}
                  house={house}
                  changeHouseInfo={() =>
                    this.props.history.push(`/updatehouse/${house.pk}`)
                  }
                />
              ))}
            </div>
          )}

        {!loading &&
          houseData.length === 0 && (
            <div style={{ textAlign: "center" }}>
              <h6>Want to be a host ?</h6>
              <h6>
                Press top right button to upload your house information ！
              </h6>
            </div>
          )}
      </Card>
    );
  }
}

const mapPropsToState = state => {
  return { houseData: state.profile.houseData, loading: state.profile.loading };
};

export default _.flow(
  withRouter,
  connect(mapPropsToState)
)(MyHouse);
